package ru.seopult.promonavigator.i18n.generator

import org.junit.Test
import static org.junit.Assert.*

/**
 * User: pyotruk
 * Date: 2015-12-15
 */

class TranslatorTests {

    @Test
    void testTranslate() {
        def r = Translator.translate(['New project', 'Password'], 'en', 'ru')

        assertEquals(2, r.size())
        assertEquals('Новый проект', r[0])
        assertEquals('Пароль', r[1])
    }

}
