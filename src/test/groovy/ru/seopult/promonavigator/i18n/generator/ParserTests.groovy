package ru.seopult.promonavigator.i18n.generator

import org.junit.Test
import static org.junit.Assert.*

/**
 * User: pyotruk
 * Date: 2015-12-15
 */

class ParserTests {

    private static final def PROJECT_PATH = '/home/pyotruk/dev/promonavigator.com.br/frontend/'

    private static final def scripts = [
            [src: PROJECT_PATH + 'st/app/services/HttpService.js', test: { r ->
                assertTrue('Saved' in r)
                assertTrue('Started' in r)
                assertTrue('Stopped' in r)
            }],
            [src: PROJECT_PATH + 'st/app/modules/project/pBudget/pBudgetController.js', test: { r ->
                assertTrue('Project budget should be greater than [min] and less than [max]' in r)
                assertTrue('Context advert for [n] keywords has started successfully. Campaign activation can take up to few hours.' in r)
            }],
            [src: PROJECT_PATH + 'st/app/modules/profile/BillingController.js', test: { r ->
                assertTrue('Balance refilled for [amount][currency] and now your balance is [balance][currency]' in r)
                assertTrue('Payment for [amount][currency] is being processed. Balance will be updated when payment conducted' in r)
                assertTrue('During the payment in the amount of [amount][currency] an error has occurred. If you need help write on billing@promonavigator.com.br' in r)
                assertTrue('Balance refill' in r)
            }],
            [src: PROJECT_PATH + 'st/app/modules/project/pWords/Manual.js', test: { r ->
                assertTrue('[n] new words added' in r)
                assertTrue('[n] words are already added' in r)
            }],
            [src: PROJECT_PATH + 'st/app/directives/validation/pnValidate.js', test: { r ->
                assertTrue('Too short, must be >= [min]' in r)
                assertTrue('Too long, must be <= [max]' in r)
            }]
    ];

    private static final def templates = [
            [src: PROJECT_PATH + 'www/templates/project/pAds/project.ads.html', test: { r ->
                assertTrue('Selected' in r)
                assertTrue('ads in' in r)
                assertTrue('words' in r)
                assertTrue('You can edit them, if necessary.' in r)
                assertTrue('Further' in r)
                assertTrue('Keywords filter' in r)
                assertTrue('All words' in r)
                assertTrue('Apply' in r)
                assertTrue('Total ads' in r)
                assertTrue('To add an advert' in r)
                assertTrue('Please wait, generating ads...' in r)
                assertTrue('Next' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/projects/projects.html', test: { r ->
                assertTrue('Settings' in r)
                assertTrue('Impressions' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pWords/project.keywords.html', test: { r ->
                assertTrue('Keywords selection in progress' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pBudget/ads.accordion.html', test: { r ->
                assertTrue('To add an advert' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pBudget/project.budget.html', test: { r ->
                assertTrue('Campaign loading...' in r)
                assertTrue('Start' in r)
                assertTrue('Stop' in r)
                assertTrue('Delete' in r)
                assertTrue('Edit minus-words' in r)
                assertTrue('You are in step campaign management. Set a budget for an advertising campaign and set the maximum rate that you are willing to pay per click for each keyword.' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pTargetings/tactics.modal.html', test: { r ->
                assertTrue('Max_Click_Description' in r)
                assertTrue('Holding_Place_Description' in r)
                assertTrue('Blast_The_Rival_Description' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pWords/competitors.html', test: { r ->
                assertTrue('Competitors_load_no_data' in r)
                assertTrue('Competitors_add_no_data' in r)
                assertTrue('add your keywords' in r)
                assertTrue('Do you know your competitor? Enter the address of his site and we will show the words on which it is moving' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/shared/fix-budget.modal.html', test: { r ->
                assertTrue('New budget' in r)
                assertTrue('mo.' in r)
                assertTrue('current budget is' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pBudget/minus-words.modal.html', test: { r ->
                assertTrue('Minus-words adding excludes ads impressions for queries that contains the minus-words.' in r)
                assertTrue('Choose minus-words from table below or enter your own words in format "-word1 - word2"' in r)
                assertTrue('How to setup minus-words properly?' in r)
                assertTrue('Minus-words editing' in r)
                assertTrue('Total minus-words list' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pAds/ads-form.accordion.html', test: { r ->
                assertTrue('Copied' in r)
                assertTrue('Copy' in r)
                assertTrue('Create' in r)
                assertTrue('Save' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/components/ad-status.html', test: { r ->
                assertTrue('Ad has not passed the moderation. Rejection reason:' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pSettings/project.settings.html', test: { r ->
                assertTrue('To start the campaign need to take a few simple configuration steps. How can you make the first step "establishment project" - it does not require any special knowledge: simply fill in the fields by following our tips.' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pBudget/bid-matrix.popover.html', test: { r ->
                assertTrue('price: to enter' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/main/main.html', test: { r ->
                assertTrue('Начать кампанию' in r)
                assertTrue('Простота' in r)
                assertTrue('Экономия времени' in r)
                assertTrue('Не требует знаний Adwords. Создание и управление рекламной кампанией полностью автоматизировано.' in r)
                assertTrue('Bid management is a hard and routine process. Why spend your time on this? Leave it to our system while taking care of your clients. Effective bid management is a key to spending your budget effectively and getting more clients.' in r)
                assertTrue('преимущества для вас' in r)
                assertTrue('Efficient company budget is from [budget] per month. The commission of service is [comission].' in r)
                assertTrue('Don’t know which keywords to choose to promote your website? Our system will analyze the content of your website and share the list of keywords which will perfectly match your goals.' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/auth/sign-in.html', test: { r ->
                assertTrue('Forgot your password?' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/auth/restore-password-change.html', test: { r ->
                assertTrue('Congratulations!' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/modals/payment-required.modal.html', test: { r ->
                assertTrue('Recommended sum (for month)' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/faq/faq.html', test: { r ->
                assertTrue('ИСКАТЬ' in r)
                assertTrue('More' in r)
                assertTrue('Hide' in r)
                assertTrue('Open in new window' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/components/balance.html', test: { r ->
                assertTrue('Money runs out in [days] days. Please add funds' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/shared/titles.html', test: { r ->
                assertTrue('Campaigns list' in r)
                assertTrue('Sign in' in r)
                assertTrue('Superuser Login' in r)
                assertTrue('Registration' in r)
                assertTrue('Ticket #[id]' in r)
                assertTrue('Referral program' in r)
                assertTrue('Статистика списаний и начислений' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/refs/refs.html', test: { r ->
                assertTrue('Refs_text_top_[refPercent]' in r)
                assertTrue('Your referral link:' in r)
                assertTrue('Refs_text_bottom' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/feedback/feedback.html', test: { r ->
                assertTrue('Tickets' in r)
                assertTrue('Department' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/project/pBudget/group-operations.html', test: { r ->
                assertTrue('Max = Position +' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/shared/header/header-not-logged-sm.html', test: { r ->
                assertTrue('регистрация (бесплатно)' in r)
            }],
            [src: PROJECT_PATH + 'www/templates/main2/main2.html', test: { r ->
                assertTrue('8h/dia' in r)
                assertTrue('Sugestão de Orçamento' in r)
                assertTrue('PromoNavigator © 2016 Todos os direitos reservados.' in r)
                assertTrue('Suporte Online 8 horas/dia' in r)
                assertTrue('suporte@promonavigator.com.br' in r)
                assertTrue('+55 11 2659-4145' in r)
                assertTrue('Agências e profissionais especializados cobram caro para realizar esse serviço, aqui a nossa comissão é de apenas 15% em cima do valor realmente gasto na campanha!' in r)

                assertEquals(138, r.size())
            }]
    ];

    @Test
    void testParseScripts() {
        scripts.each {
            def r = Parser.parse(new File(it.src));
            it.test(r);
        }
    }

    @Test
    void testParseTemplates() {
        templates.each {
            def r = Parser.parse(new File(it.src));
            it.test(r);
        }
    }

}
