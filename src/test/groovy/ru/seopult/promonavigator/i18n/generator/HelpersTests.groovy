package ru.seopult.promonavigator.i18n.generator

import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * User: pyotruk
 * Date: 2015-12-15
 */

class HelpersTests {

    @Test
    void testReplaceDoubleQuotes() {
        def s = /Choose minus-words from table below or enter your own words in format "-word1 - word2"/
        s = Helpers.replaceDoubleQuotes(s)

        assertEquals("Choose minus-words from table below or enter your own words in format '-word1 - word2'", s)
    }

    @Test
    void testParseLangCode() {
        def lang = Helpers.parseLangCode('pt_BR')
        assertEquals('pt', lang)
    }

}
