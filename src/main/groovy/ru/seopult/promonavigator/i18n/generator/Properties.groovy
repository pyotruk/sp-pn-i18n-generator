package ru.seopult.promonavigator.i18n.generator

import java.text.MessageFormat

/**
 * User: pyotruk
 * Date: 2015-12-05
 */

class Properties {

    private final static String PROPERTIES_PATH = '/config.properties'

    private static java.util.Properties props = new java.util.Properties()

    static {
        props.load(Properties.getResourceAsStream(PROPERTIES_PATH))
    }

    public static String get(String name, Object... args) {
        return MessageFormat.format(props."$name" as String, args)
    }

}
