package ru.seopult.promonavigator.i18n.generator

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.util.regex.Pattern

/**
 * User: pyotruk
 * Date: 2015-12-15
 */

class Parser {

    private static final Logger log = LoggerFactory.getLogger(Parser.class)


    static private final String ALLOWED_KEY_SYMBOLS = '\\w\\d\\.\\-\\+\\?\\(\\)\\[\\]"`_,:!#’@=><\\/©% ';

    static private final String JS_PTRN = Pattern.compile(

            // modifiers
            '(?msU)' + // U-capital is unicode modifier in Java

            // $translate.instant('Started')
            // $translate.instant('Context advert for [n] keywords has started successfully', {n: keywords.size()})
            '(?:translate\\.instant\\(\\s*?\'([' + ALLOWED_KEY_SYMBOLS + ']+)\'.*?\\))' +
            '|' +

            // Lang.translate('[n] words are already added', {n: existed}, function (t) {...})
            '(?:Lang\\.translate\\(\\s*?\'([' + ALLOWED_KEY_SYMBOLS + ']+)\'.*?\\))'
    )

    static private final String HTML_PTRN = Pattern.compile(

            // modifiers
            '(?msU)' + // U-capital is unicode modifier in Java

            // {{'Competitors_load_no_data' | translate}}
            '(?:\\{\\{ ?\'([' + ALLOWED_KEY_SYMBOLS + ']+)\' \\| translate ?\\}\\})' +
            '|' +

            // {{ 'преимущества для вас' | translate | uppercase }}
            '(?:\\{\\{ ?\'([' + ALLOWED_KEY_SYMBOLS + ']+)\' \\| translate \\| [\\w]+ ?\\}\\})' +
            '|' +

            // {{'Competitors_add_no_data' | translate:'{host: "' + competitors.new + '"}'}}
            '(?:\\{\\{\'([' + ALLOWED_KEY_SYMBOLS + ']+)\' \\| translate:\'\\{.*?\\}\'\\}\\})' +
            '|' +

            // <a title="{{'Money runs out in [days] days. Please add funds' | translate:leftDaysTranslation}}"></a>
            '(?:\\{\\{\'([' + ALLOWED_KEY_SYMBOLS + ']+)\' \\| translate:[\\w]+\\}\\})' +
            '|' +

            // <p translate="Max_Click_Description"></p>
            '(?:translate="([' + ALLOWED_KEY_SYMBOLS + ']+)")' +
            '|' +

            // <i title="{{ad.data.started ? ('Started' | translate) : ('Stopped' | translate)}}"></i>
            '(?:\\(\'([' + ALLOWED_KEY_SYMBOLS + ']+)\' \\| translate\\))' +
            '|' +

            // <h4>{{(minusWords.isTotal ? 'Total minus-words list' : 'Minus-words editing') | translate}}</h4>
            '(?:\\{\\{\\([^\\{\\}]*? \\? \'([' + ALLOWED_KEY_SYMBOLS + ']+)\' : \'([' + ALLOWED_KEY_SYMBOLS + ']+)\'\\) \\| translate\\}\\})'
    )

    static List parse(File f) throws IllegalArgumentException {
        if (f.name.endsWith('.js')) {
            return parseScript(f)
        } else if (f.name.endsWith('.html')) {
            return parseTemplate(f)
        } else {
            throw new IllegalArgumentException("Unexpected file extension [${f.path}]")
        }
    }

    private static List parseScript(File f) {
        log.info("Parsing JS-file [$f.path] with regexp: $JS_PTRN")
        return _parse(f, JS_PTRN)
    }

    private static List parseTemplate(File f) {
        log.info("Parsing HTML-file [$f.path] with regexp: $HTML_PTRN")
        return _parse(f, HTML_PTRN)
    }

    private static List _parse(File f, String pattern) {
        def m = f.text =~ pattern
        def r = [];

        while (m.find()) {
            m.groupCount().times { i ->
                def g = m.group(i + 1)
                if (g) r << g
            }
        }
        return r
    }

}
