package ru.seopult.promonavigator.i18n.generator

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * User: pyotruk
 * Date: 2015-12-15
 */

class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class)

    public static void main(String[] args) {
        def projectPath = args[0] as String
        def i18nPath = args[1] as String

        play(projectPath, i18nPath)
    }

    private static play(String projectPath, String i18nPath) {
        def srcLang = Properties.get('translator.lang.src')

        def project = new Project(projectPath, i18nPath)
        def parsed = project.parse().unique()

        log.debug("Parsed >>> \n $parsed \n <<<.")

        Properties.get('translator.lang.list').split(',').each { String lang ->
            def translated = parsed
            if (!lang.equals(srcLang)) {
                translated = Translator.translate(
                        parsed,
                        Helpers.parseLangCode(srcLang),
                        Helpers.parseLangCode(lang)
                )

                log.debug("Translated ($lang) >>> \n $translated \n <<<.")
            }

            def r = [:]
            parsed.eachWithIndex { it, i ->
                r.put(it, Helpers.replaceDoubleQuotes(translated[i]))
            }

            project.saveI18n(r, lang)
        }
    }

}
