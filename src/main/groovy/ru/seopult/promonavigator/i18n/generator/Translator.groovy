package ru.seopult.promonavigator.i18n.generator

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import groovy.json.JsonSlurper
import org.jsoup.Jsoup

/**
 * User: pyotruk
 * Date: 2015-12-15
 */

class Translator {

    private final static String API_URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

    static List<String> translate(List<String> text, String srcLang, String dstLang) {
        def params = ArrayListMultimap.create()

        params.put('key', Properties.get('translator.yandex.api.key'))
        params.put('lang', "$srcLang-$dstLang")

        text.each {
            params.put('text', it)
        }

        def json = Jsoup.connect(API_URL + paramsToQueryString(params))
                .ignoreContentType(true).post().text()

        def data = new JsonSlurper().parseText(json)

        return data.text;
    }

    static private String paramsToQueryString(Multimap params) {
        def r = '?';
        params.entries().each {
            r += "${it.key}=${it.value}&"
        }
        return r
    }

}
