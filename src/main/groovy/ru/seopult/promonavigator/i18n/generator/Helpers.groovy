package ru.seopult.promonavigator.i18n.generator

/**
 * User: pyotruk
 * Date: 2015-12-15
 */

class Helpers {

    static String parseLangCode(String lang) {
        return lang.substring(0, 2)
    }

    static String replaceDoubleQuotes(String s) {
        return s.replaceAll(/"/, "'")
    }

}
