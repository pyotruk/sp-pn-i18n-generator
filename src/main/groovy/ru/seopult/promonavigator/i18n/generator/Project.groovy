package ru.seopult.promonavigator.i18n.generator

import groovy.json.JsonBuilder
import groovy.json.StringEscapeUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * User: pyotruk
 * Date: 2015-12-15
 */

class Project {

    private static final Logger log = LoggerFactory.getLogger(Project.class)

    private final File root
    private final List<String> ignoreList
    private final File i18nDir

    private final static List<String> ALLOWED_EXT = Properties.get('project.allowed.ext').split(',')

    Project(String path, String i18nPath) {
        this.root = new File(path)
        this.i18nDir = new File(this.root.absolutePath + i18nPath)

        this.ignoreList = (new File(this.root.absolutePath + Properties.get('project.ignore.vcs'))).text.split("\n")
        this.ignoreList.addAll(Properties.get('project.ignore.dirs').split(','))
    }

    List<String> parse() {
        def r = []

        ; { dir ->
            dir.eachFile { f ->
                log.debug("Current file [${f.path}].")

                if (!checkFileIsAllowed(f)) {
                    log.info("File [$f.path] is ignored.")
                    return
                }

                if (f.isDirectory()) {
                    owner.call(f)

                } else {
                    try {
                        r += Parser.parse(f)

                    } catch (Throwable t) {
                        log.error('Parser error', t)
                    }
                }
            }
        }.call(this.root)

        return r
    }

    private boolean checkFileIsAllowed(File f) {
        if (f.isFile()) {
            if (!ALLOWED_EXT.find { ext -> f.name.endsWith(ext) }) {
                return false
            }
        }
        if (ignoreList.find { (f.absolutePath + '/').contains(it) }) {
            return false
        }
        return true
    }

    void saveI18n(Map i18nData, String lang) {
        def f = new File(this.i18nDir.absolutePath + "/${lang}.json")
        f.text = StringEscapeUtils.unescapeJavaScript( // unescape UTF-8
                new JsonBuilder(i18nData).toPrettyString()
        )
        log.info("I18n output file saved to [${f.absolutePath}]")
    }

}
